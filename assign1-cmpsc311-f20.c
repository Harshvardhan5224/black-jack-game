////////////////////////////////////////////////////////////////////////////////
//
//  File           : cmpsc311-f16-assign1.c
//  Description    : This is the main source code for for the first assignment
//                   of CMPSC311 at Penn State University.  See the related
//                   assignment page for details.
//
//   Author        : Harshvardhan Sharma
//   Last Modified : 2:55pm September 16
//

// Include Files

#include <stdio.h>

#include "cmpsc311_util.h"

#include <time.h>

// Defines
#define NUM_CARDS 52
#define MAX_CARDS 11
#define SPADE   "\xE2\x99\xA0"
#define CLUB    "\xE2\x99\xA3"
#define HEART   "\xE2\x99\xA5"
#define DIAMOND "\xE2\x99\xA6"

// Functions

////////////////////////////////////////////////////////////////////////////////
//
// Function     : print_card
// Description  : print the card from the integer value
//
// Inputs       : card - the card to print
// Outputs      : 0 always


// remove spacing
// proper allignment 

int print_card( int card ) {

    // CODE HERE
    char card_faces[] = "234567891JQKA";
    char *card_suits[] = { SPADE, CLUB, HEART, DIAMOND };
    int suit = card / 13, cardty = card % 13;
    if ( cardty == 8 ) {
        printf("10%s", card_suits[suit] );
    } else {
        printf("%c%s", card_faces[cardty], card_suits[suit] );
    }

    // Return zero
    return( 0 );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : print_cards
// Description  : print a number of cards (no more than 13 on one line)
//
// Inputs       : cards - the array of cards to print
//                num_cards - the number of cards to print
// Outputs      : 0 always

int print_cards( int cards[], int num_cards ) 
{   
    // prints 13 cards per line
    for (int i=0;i<num_cards;i++)

    
    {  
         if (i%13==0 && i != 0)
    {
        printf("\n");
    }
        print_card(cards[i]);
        printf("  ");
    }
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : shuffle_cards
// Description  : print a number of cards (no more than 13 on one line)
//
// Inputs       : cards - the array of cards to print
//                num_cards - the number of cards to print
// Outputs      : 0 always
uint32_t getRandomValue( uint32_t min, uint32_t max );
int shuffle_cards( int cards[], int num_cards ) 
{
    for (int i = 1; i < num_cards; i++) 
    {    
        // this is used to get the random card number
        int j = getRandomValue(0,i);

        // uint32_t getRandomValue( uint32_t min, uint32_t max ); // Using strong randomness, generate random number
        // swapping the values cards in the deck   
        int t = cards[i];
        cards[i] = cards[j];
        cards[j] = t; 
            
    }   
    return 0;
}


////////////////////////////////////////////////////////////////////////////////
//
// Function     : hand_value
// Description  : return the value of the hand
//
// Inputs       : cards - the array of cards in the hand
//                num_cards - the number of cards in the hand
// Outputs      : 0 always

int hand_value( int cards[], int num_cards ) 
{
    // creating an array for all card values
    int values[] = {2,3,4,5,6,7,8,9,10,10,10,10,11};
    int i,sum=0,id,aces=0;
    for (i=0;i<num_cards;i++)
    {
        id = cards[i]%13;
        sum = sum + values[id];
        if (id==12)
        {
            //for counting aces in the hand
            aces++;
        }
    }
    
    // checking for a blackjack
    if (sum == 21 && num_cards == 2)
    {
        return 1;
    }

    else
    {
        while ( aces>=1 && sum > 21)
        // adjusting value of ace to 1 or 11, best possible scenario. 
        {
            sum = sum - 10;
            aces--;
            if (sum>=17 && sum<=21 && aces==0)
                break;
        }

        return sum;
    }
        

    
    
}

 

////////////////////////////////////////////////////////////////////////////////
//
// Function     : sort_cards
// Description  : sort a collection of cards
//
// Inputs       : hand - the cards to sort
//                num_cards - the number of cards in the hand
// Outputs      : 0 always

int sort_cards( int hand[], int num_cards ) 
{
    int last,i,temp,num1,num2,suit1,suit2,flag;
    // sorting cards with face values using bubble sort
    for (last=num_cards-1;last>0;last--)
    {   
        flag=0;// to indicate no swapping required
        for (i=0;i<last;i++)

        {
            num1 = hand[i]%13;
            num2 = hand[i+1]%13;
            if (num2<num1)
            {
                flag=1;
                temp = hand[i];
                hand[i] = hand[i+1];
                hand[i+1] = temp; 
            } 
        }
        if (flag == 0)
        {
            break;
        }

    }
    
    // sorting cards with SUIT values
    for (last=num_cards-1;last>0;last--)
    {
        flag=0;
        for (i=0;i<last;i++)
        {   
            num1 = hand[i]%13;
            num2 = hand[i+1]%13;
            suit1 = hand[i]/13;
            suit2 = hand[i+1]/13;
            if (num2==num1 && suit2<suit1)
            {
                flag=1;
                temp = hand[i];
                hand[i] = hand[i+1];
                hand[i+1] = temp; 
            } 
        }
        if (flag == 0)
        {
            break;
        }

    }
    

    return 0;



    

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : dealer_play
// Description  : dealer decision to hit or stand (hit on anything less 
//                than 17)
//
// Inputs       : hand - cards dealer has
//                num_cards - the number of cards in player hand
// Outputs      : 0 = stand, 1 = hit

int dealer_play( int hand[], int num_cards ) 
{
    // checking conditions for hit or stand
    int value = hand_value(hand,num_cards); 
    if (value > 16 && value <= 21)
    {
        // 0 for stand
        return 0;
    }
    else
    {
        // 1 for hit
        return 1;
    }

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : player_play
// Description  : player decision to hit or stand
//
// Inputs       : hand - cards player has
//                num_cards - the number of cards in player hand
//                dealer_card - the dealers face up card
// Outputs      : 0 = stand, 1 = hit

int player_play( int hand[], int num_cards, int dealer_card ) 
{
    // checking conditions for hit or stand
    int value = hand_value(hand,num_cards); 
    if (value > 16 && value <= 21)
    {
        // 0 for stand
        return 0;
    }
    else
    {
        // 1 for hit
        return 1;
    }

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : play_hand
// Description  : play a hand of black jack
//
// Inputs       : deck - the whole deck of cards
//                num_cards - the number of cards in the deck
//                player_money - pointer to player amount of money
// Outputs      : 1 if player wins, 0 if dealer wins

int play_hand( int deck[], int num_cards, float *player_money ) 
{
    int player_cards[10], dealer_cards[10], card_countp=0,card_countd=0,playersum=0,dealersum=0,play;
    shuffle_cards(deck,num_cards);
    printf("\n----New hand----\n");
    
    //  dealer draws 2 cards and prints the cards (second card is hidden)
    printf("\nDealer cards: ");
    dealer_cards[0] = deck[num_cards-1];
    print_card(dealer_cards[0]);
    printf(" ");
    card_countd++;
    num_cards--;
    dealer_cards[1] = deck[num_cards-1];
    printf("XX");
    printf(" ");
    card_countd++;
    num_cards--;    

    //player draws 2 cards and  print the cards
    printf("\n\nPlayer cards: ");
    player_cards[0] = deck[num_cards-1];
    print_card(player_cards[0]);
    printf(" ");
    card_countp++;
    num_cards--;
    player_cards[1] = deck[num_cards-1];
    print_card(player_cards[1]);
    card_countp++;
    num_cards--;    

    // calculating the sum for dealer and player
    playersum = hand_value(player_cards,card_countp);
    dealersum = hand_value(dealer_cards,card_countd);

    //checking for blackjack
    if (playersum == 1)
    {
        printf("\nPlayer has Blackjack!, wins $7.50\n");
        
        *player_money = *player_money + 12.50;
        return 1;
    }

    do
    {
        //to check if the player hits or stands
        // when play ==1;  this implies that the player will play and if zero then the player stands
        play = player_play(player_cards,card_countp,dealer_cards[1]);
        if(play == 1)
        {
            //player draws a card and then checks the value of the hand again
            player_cards[card_countp] = deck[num_cards];
            card_countp++;
            num_cards--;
            playersum = hand_value(player_cards,card_countp);
            printf("\nPlayer hit (%d): ", playersum);
            print_cards(player_cards,card_countp);   
            if (playersum > 21)
            {
                // player loses when the sum is above 21
                printf("\nPlayer Busts ... dealer Wins!\n");
                return 0;
            }
        }

    else
    {
        printf("\nPlayer Stands (%d) :",playersum);
        print_cards(player_cards,card_countp);
    }
        
    
    }  while (play == 1);


    do
    {
        //to check if the dealer hits or stands
        // when play ==1;  this implies that the player will play and if zero then the player stands
        play = dealer_play(dealer_cards,card_countd);
        if(play == 1)
        {
            //dealer draws a card and then checks the value of the hand again
            dealer_cards[card_countd] = deck[num_cards];
            card_countd++;
            num_cards--;
            dealersum = hand_value(dealer_cards,card_countd);
            printf("\nDealer hit (%d): ", dealersum);
            print_cards(dealer_cards,card_countd);   
            if (dealersum > 21)
            {
                // dealer loses when the sum is above 21
                printf("\nDealer Busts ... Player Wins!\n");
                *player_money = *player_money + 10.00;
                return 0;
            }
        }

    else
    {
        printf("\nDealer Stands (%d) :",dealersum);
        print_cards(dealer_cards,card_countd);
    }

    }  while (play == 1);

    // comparing the final hand values and declairing the winner for the round
    if (dealersum >= playersum)
    {
        printf("\nDealer wins !!! \n");
        return 1;
    }    
    else
    {
        printf("\nPlayer wins !!!\n");
        *player_money = *player_money + 10.00;
        return 0;
    }

    

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : show_player_money_histogram
// Description  : Display a histogram of the player funds by hands
//
// Inputs       : money_rounds - the list of player money by hand number
//                last_round - the hand number where the game stopped
// Outputs      : 0 always

int show_player_money_histogram( float money_rounds[], int last_round ) 
{
    //Create a 2d array
    char graph[100][41];
    int i, row, col;
    int step ;
    // The for loop is making vertical graph lines in the form of ('X' and '.') 
    for (row = 0;row<last_round;row++)
    {
        step = money_rounds[row]/5;
        for(col = 0; col<=step;col++)
        {
            graph[row][col] = 'X';
        }
        for(col = step+1 ;col<=40; col++)
        {
            graph[row][col] = '.';
        }
    }

    // when the player has lost all money before 100 rounds
    // for loop stores only '.' in vertical lines to show that the player lost.
    for(row=last_round; row<100; row++)
    {
        graph[row][0] = 'X';
        for(col = 1 ;col<=40; col++)
        {
            graph[row][col] = '.';
        }
    }


    printf("\n                                               Player Cash by Round\n");
    printf("     ");
    for (i=1; i<=100;i++)
    {
        printf("-");
    }
    printf("\n");


    // printing the histogram
    for( col = 40,i=200 ; col>=0; col--, i-=5)
    {
        printf("%3d |",i);
        for (row = 0; row < 100; row++)
        {
            printf("%c",graph[row][col]);
        }
        printf("|\n");
    }

    //  to make the border line
    printf("     ");
    for (i=1; i<=100;i++)
    {
        printf("-");
    }
    printf("\n");

    // prints numbers 1-10;  each number is printed after 10 rounds ; scale
    printf("     ");
    for (i=1; i<=100;i++)
    {
        if( i%10 == 0 )
            printf("%d", i/10);
        else
         printf(" ");
    }
    // prints 1-9 then 0 for ten times 
    printf("\n");
    printf("     ");
    for (i=1; i<=100;i++)
    {
        if( i%10 == 0 )
            printf("0");
        else
         printf("%d", i%10);
    }
    printf("\n");


return 0;
}


////////////////////////////////////////////////////////////////////////////////
//
// Function     : main
// Description  : The main function for the CMPSC311 assignment #1
//
// Inputs       : argc - the number of command line parameters
//                argv - the parameters
// Outputs      : 0 if successful test, -1 if failure


int main( int argc, char **argv ) 
{

    /* Local variables */
    int cmp311_deck[NUM_CARDS] ;  // This is the deck of cards
    
    // creating the deck of cards sequentially 
    for(int i=0;i<52;i++)
    {
        cmp311_deck[i] = i;
    }
    
    
    /* Preamble information */


    printf( "CMPSC311 - Assignment #1 - Fall 2020\n\n" );
    srand(time(NULL)) ;

    

    /* Step #2 - print the deck of cards */
        
      printf("\n\n");
      print_cards(cmp311_deck,NUM_CARDS);

    /* Step #4 - shuffle the deck */

        shuffle_cards(cmp311_deck,NUM_CARDS);


    /* Step #5 - print the shuffled deck of cards */
        printf("\n\n");   
        print_cards(cmp311_deck,NUM_CARDS);

    /* Step #6 - sort the cards */

    sort_cards(cmp311_deck,NUM_CARDS);
    printf("\n\n");
    

    /* Step #7 - print the sorted deck of cards */

    print_cards(cmp311_deck,NUM_CARDS);


    /* Step #9 - deal the hands */
    // counter keeps the count for wins of the player; arraymoney holds the amount of money with player after each round
    int counter=0;
    int i;
    float arraymoney[100];
    float player_money = 100;
    //playing 100 hands
    for (i=1;i<=100;i++)
    {
        player_money = player_money - 5;
        counter = counter + play_hand(cmp311_deck,NUM_CARDS,&player_money);
        printf("\nAfter hand %d player has %6.2f$ left\n",i,player_money);
       
        arraymoney[i-1] = player_money;
        if (player_money< 5 )
        break;

    }

    
    

    /* Step 10 show historgrapm */
    //check this part!!
    printf("-------------\n");
    printf("Blackjack done - player won %d out of 100 hands (%.2f)",counter,(float)counter);
    show_player_money_histogram(arraymoney,i -1 );
    


    /* Exit the program successfully */
    printf( "\n\nCMPSC311 - Assignment #1 - Spring 2020 Complete.\n" );
    return( 0 );
}

